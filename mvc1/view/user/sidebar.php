            <!--Sidebar Navigation-->
			<div class="am-left-sidebar">
                <div class="content">
                    <div class="am-logo"></div>
                    <ul class="sidebar-elements">
                        <li class="parent"><a href="<?php echo $site_url; ?>/user/dashboard/"><i class="icon s7-monitor"></i><span>Dashboard</span></a></li>
                        <li class="parent"><a href="<?php echo $site_url; ?>/user/logout/"><i class="icon s7-back-2"></i><span>Log Out</span></a></li>
					</ul>
                    <!--Sidebar bottom content-->
                </div>
            </div>
			<!--End Sidebar Navigation-->
