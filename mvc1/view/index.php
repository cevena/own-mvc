<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Aplus</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/css/form-elements.css">
        <link rel="stylesheet" href="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/css/style.css">
        <link rel="shortcut icon" href="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>
        <!-- Top content -->
        <div class="top-content">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1>LOGO</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4 text">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login</h3>
                        		</div>
                            </div>
                            <div class="form-bottom">
                                <span style="color:red">
                                    <?php
                                    if(isset($_SESSION['msg']))
                                    {
                                        echo $_SESSION['msg'];
                                        $_SESSION['msg']='';
                                    }
                                    ?>
                                </span>
			                    <form role="form" action="<?php echo $GLOBALS["site_url"];?>/login/submit/" method="post" class="registration-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-first-name">Email</label>
			                        	<input type="email" name="email" placeholder="Email" class="form-control" id="form-first-name" required>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-last-name">password</label>
			                        	<input type="password" name="password" placeholder="Password" class="form-control" id="form-last-name" required	>
			                        </div>
                                    <div align="right"><button type="submit" name="login" class="btn">Sign me in!</button></div>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Javascript -->
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/js/jquery.backstretch.min.js"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/js/retina-1.1.0.min.js"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/js/scripts.js"></script>
        <!--[if lt IE 10]>
            <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/login_assets/js/placeholder.js"></script>
        <![endif]-->
    </body>
</html>
