<html>
    <head>
        <link type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/stroke-7/style.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/select2/css/select2.min.css"/>
        <style type="text/css">
            .modal-title{color:#FFF;}
            .bg{background-color:transparent !important;}
        </style>
        <link rel="stylesheet" type="text/css" href="dist/bootstrap-clockpicker.min.css">
    </head>
    <body class="bg">
        <div class="md-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #2aaadd;">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close closenow" style="color:#fff;font-size:40px;"><i class="icon s7-close"></i></button>
                <h3 class="modal-title">Add Phrase</h3>
            </div>
            <?php
            if(isset($editphrase))
            {
                foreach ($editphrase as $value)
                {
                    $Phrase = $value['Phrase'];
                    $Phrase = $value['Phrase'];
                }
            }

            if($mode=='delete')
            {
            ?>
            <form action="<?php echo $GLOBALS["site_url"]."/admin/deletephrase/".$id;?>" method="post"  enctype="multipart/form-data">
                <div class="modal-body form" style="background-color: #fff; color:#000;max-height:470px;overflow-x: hidden;">
                    <div class="text-center">
                        <p>Are you confirm? Want to delete it.</p>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="Submit" class="btn btn-primary" name="delete_form">Delete</button>
                <button type="button" class="btn btn-primary closenow" >Cancel</button>
                </div>
            </form>
            <?php
            }
            else
            {
            ?>
            <form action='<?php if(isset($editphrase)){ echo $GLOBALS["site_url"]."/admin/editphrase/".$id;}else{echo $GLOBALS["site_url"]."/admin/addphrase/";} ?>' method="post"  enctype="multipart/form-data">
                <div class="modal-body form" style="background-color: #fff; color:#000;max-height:470px;overflow-x: hidden;">
                    <div class="form-group">
                        <label>Phrase</label>
                        <textarea name="phrase" class="form-control" required><?php if(isset($Phrase)){echo $Phrase;}?></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="Submit" class="btn btn-primary" name="new_form">Submit</button>
                    <button type="button" class="btn btn-primary closenow" >Cancel</button>
                </div>
            </form>
            <?php
            }
            ?>
        </div>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js" type="text/javascript"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/js/main.js" type="text/javascript"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/parsley/parsley.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="dist/bootstrap-clockpicker.min.js"></script>
        <script type="text/javascript">
            $('.clockpicker').clockpicker({
            placement: 'bottom',
            align: 'left',
            donetext: 'Done'
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
            	//initialize the javascript
            	App.init();
            	$('form').parsley();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
            	//initialize the javascript
            	App.init();
            	App.formElements();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){//initialize the javascript
            	App.init();
            	$('.md-trigger').modalEffects();
            });
        </script>
        <script type="text/javascript">
            function closeModal(){

            		parent.$('#form-primary').modal('hide');

            };

            $('.closenow').on("click", function () {

                    closeModal();

                });
        </script>
    </body>
</html>
