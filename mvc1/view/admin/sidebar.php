            <!--Sidebar Navigation-->
			<div class="am-left-sidebar">
                <div class="content">
                    <div class="am-logo"></div>
                    <ul class="sidebar-elements">
                        <li class="parent"><a href="<?php echo $GLOBALS["site_url"]; ?>/admin/dashboard/"><i class="icon s7-monitor"></i><span>Dashboard</span></a></li>
						<li class="parent"><a href="<?php echo $GLOBALS["site_url"]; ?>/admin/users/"><i class="icon s7-user"></i><span>Users</span></a></li>
						<li class="parent"><a href="<?php echo $GLOBALS["site_url"]; ?>/admin/phrases/"><i class="icon s7-note2"></i><span>Phrases</span></a></li>
                        <li class="parent"><a href="<?php echo $GLOBALS["site_url"]; ?>/admin/logout/"><i class="icon s7-back-2"></i><span>Log Out</span></a></li>
					</ul>
                    <!--Sidebar bottom content-->
                </div>
            </div>
			<!--End Sidebar Navigation-->
