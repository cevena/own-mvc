<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/img/favicon.html">
        <title><?php echo $title;?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/stroke-7/style.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.nanoscroller/css/nanoscroller.css"/>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.magnific-popup/magnific-popup.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.niftymodals/css/component.css"/>
        <link rel="stylesheet" type="text/css"  href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link rel="stylesheet" type="text/css"  href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/select2/css/select2.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/dropzone/dist/dropzone.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.gritter/css/jquery.gritter.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.fullcalendar/fullcalendar.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.fullcalendar/fullcalendar.print.css"/>
        <link type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/css/style.css" rel="stylesheet">
        <link type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/percent.chart/chart.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/css/dataTables.bootstrap.min.css"/>
        <style type="text/css">
            .modal-title{color:#FFF;}
            .bg{background-color:transparent !important;}
        </style>
        <style type="text/css">
            .dark-anchor a
            {
            color:#FFF;
            }
            .dark-anchor a:hover
            {
            color:#000;
            }
            input[type=checkbox].css-checkbox {
            position:absolute; z-index:-1000; left:-1000px; overflow: hidden; clip: rect(0 0 0 0); height:1px; width:1px; margin:-1px; padding:0; border:0;
            }
            input[type=checkbox].css-checkbox + label.css-label {
            padding-left:22px;
            height:17px;
            display:inline-block;
            line-height:17px;
            background-repeat:no-repeat;
            background-position: 0 0;
            font-size:17px;
            vertical-align:middle;
            cursor:pointer;
            }
            input[type=checkbox].css-checkbox:checked + label.css-label {
            background-position: 0 -17px;
            }
        </style>
    </head>
