<?php
session_start();

$protocol = 'http';

$GLOBALS["site_url"] = $site_url = $protocol . "://" . $_SERVER['HTTP_HOST'].'/manifest_infotech/OwnFrameWork/mvc1';

include_once("config/MySqlHelper.php");
$uriGetParam = isset($_GET['uri'])? $_GET['uri'] : "/";
$uri = array();

if($uriGetParam=="/")
{
    include_once("controller/login.php");
    $ctrl_obj = new Login;
    $method = 'login';
}
else {
    $url_string = trim($uriGetParam, '/');
    $uri = explode('/',$url_string);
    $controller = $uri[0];
    $method = isset($uri[1])?$uri[1]:'defaultmethod';

    array_shift($uri);
    array_shift($uri);

    $parameters = implode(',',$uri);

    include_once("controller/".$controller.".php");

    $ctrl_obj = new $controller;
}
call_user_func_array(array($ctrl_obj, $method), $uri);
