<?php
include_once("model/user.php");

class User{
	public $model;

	public function __construct()
    {
		if(!isset($_SESSION['UserId']) OR $_SESSION['is_admin']!='0')
		{
		   header("location:".$GLOBALS["site_url"]);
		}
		$this->model = new Userdata();
    }

	public function dashboard()
	{
		$users = $this->model->getuser($_SESSION['UserId']);
		foreach($users as $user)
		$title = $user['UserName'];

		if(isset($_POST['name']))
		{
			$name = $_POST['name'];
			$phrase = $this->model->getphrases();
			foreach($phrase as $value)
			$mood_string = str_replace("%name%", $name, $value['Phrase']);
		}

		include 'view/common/header.php';
		include 'view/user/sub_header.php';
		include 'view/user/sidebar.php';
		include 'view/user/dashboard.php';
		include 'view/user/footer.php';
	}

	public function logout()
	{
		unset($_SESSION['UserId']);
		unset($_SESSION['is_admin']);
		session_destroy();
		header("location:".$GLOBALS["site_url"]);
	}
}
