
jQuery(document).ready(function() {
    var site_url = location.protocol + '//' + location.host + '/ManifestInfotech/OwnFrameWork/mvc1/';
    /*
        Fullscreen background
    */
    $.backstretch([site_url + "/assets/login_assets/img/backgrounds/1.jpg"], {duration: 3000, fade: 750});

    $('#top-navbar-1').on('shown.bs.collapse', function(){
    	$.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$.backstretch("resize");
    });

    /*
        Form validation
    */
    $('.registration-form input[type="text"], .registration-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });

    $('.registration-form').on('submit', function(e) {

    	$(this).find('input[type="text"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});

    });


});
