<?php
Class MySqlHelper
{
    public $con;
    public $server = "localhost";
    public $user = "root";
    public $password = "";
    public $db = "framework";

    private $table_name = "";
    private $where_string = "";
    private $join_string = "";
    private $orderby_string = "";
    private $identity_field = "";
    private $dbh;

    function __construct()
    {
        try {
            $this->dbh = new PDO('mysql:host=' . $this->server . ';dbname=' . $this->db, $this->user, $this->password);
        }
        catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function Insert($querydata)
    {
        $this->ResetVariables();
        $table_name     = $querydata["tablename"];
        $row_data       = array();
        $row_data       = $querydata["rowdata"];
        $identity_field = $querydata["identity"];
        $strsql         = "";
        $strsql         = "INSERT INTO " . $table_name . "(";
        $strfields      = array();
        $strparams      = array();
        $strvalues      = array();
        foreach ($row_data as $key => $value)
		{
            $strfields[] = $key;
            $strparams[] = ":$key";
        }
        $strsql = $strsql . join(",", $strfields) . ")values(" . join(",", $strparams) . ")";
        $stmt = $this->dbh->prepare($strsql);

        foreach ($row_data as $key => $value)
		{
            $stmt->bindValue(':' . $key, $value, PDO::PARAM_STR);
        }
        $stmt->execute();
        $identity_value = $this->dbh->lastInsertId();

        return $identity_value;
    }

    public function Update($querydata)
    {
        $this->ResetVariables();
        $table_name     = $querydata["tablename"];
        $row_data       = array();
        $row_data       = $querydata["rowdata"];
        $identity_field = $querydata["identity"];
        $strsql         = "UPDATE " . $table_name . " SET ";
        $strfields      = array();
        $strparams      = array();
        $strvalues      = array();
        foreach ($row_data as $key => $value) {
            if ($key != $identity_field)
            $strfields[] = "$key=:$key";
        }
        $strsql = $strsql . join(",", $strfields);
        $strsql = $strsql . ' Where ' . $identity_field . '=:' . $identity_field;

		$stmt = $this->dbh->prepare($strsql);

        foreach ($row_data as $key => $value)
		{
            $stmt->bindValue(':' . $key, $value, PDO::PARAM_STR);
        }
        $stmt->execute();
	}

    public function Delete($tablename, $where)
    {
        $this->ResetVariables();
        $strsql = "DELETE FROM " . $tablename;
        if (!empty($where))
		{
        	$strsql = $strsql . ' Where ' . $where;
        }
		$stmt = $this->dbh->prepare($strsql);
		$stmt->execute();
	}

    public function GetRows($tablename, $columns, $where)
    {
        $strsql = "Select " . $columns . " From " . $tablename . " Where " . $where;
        $rows   = $this->dbh->query($strsql);
        return $rows;
    }

    public function GetRow($tablename, $columns)
    {
        $strsql = "Select " . $columns . " From " . $tablename;
        $rows   = $this->dbh->query($strsql);
		return $rows;
    }

    public function GetSingle($tablename, $columns, $where)
    {
        $strsql = "Select " . $columns . " From " . $tablename . " Where " . $where;
        $rows   = $this->dbh->query($strsql);
        $f      = $rows->fetch();
        $result = $f[$columns];

        return $result;
    }

    public function GetSingleByFormula($tablename, $formula, $columns, $where)
    {
        $strsql = "Select " . $formula . " From " . $tablename . " Where " . $where;
        $rows   = $this->dbh->query($strsql);
        $f      = $rows->fetch();
        $result = $f[$columns];
		return $result;
    }

    public function GetRowsByQuery($query)
    {
        $rows = $this->dbh->query($query);
		return $rows;
    }

    private function ResetVariables()
	{
		$table_name ="";
		$where_string ="";
		$join_string ="";
		$orderby_string ="";
		$identity_field ="";
	}
}
?>
