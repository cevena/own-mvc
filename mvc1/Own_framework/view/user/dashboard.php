<div class="am-content">
	<div class="page-head">
		<h2>Welcome, <?php echo $title;?></h2>
	</div>
    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget widget-pie widget-pie-stats">
					<form action="<?php echo $GLOBALS["site_url"]."/user/dashboard/"?>" method="post"  enctype="multipart/form-data">
		                <div class="modal-body form" style="background-color: #fff; color:#000;max-height:470px;overflow-x: hidden;">
							<div class="form-group">
		                        <label>Enter Name</label>
		                        <input type="text" name="name" class="form-control " required>
		                    </div>
		                </div>
		                <div class="modal-footer">
		                <button type="Submit" class="btn btn-primary" name="delete_form">Get Mood</button>
		                </div>
		            </form>
					<?php
					if(isset($mood_string))
					{
					?>
					<div class="panel-body">
		                <div role="alert" class="alert alert-primary alert-icon alert-dismissible">
			                <div class="icon"><span class="s7-check"></span></div>
			                <div class="message">
			                	<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>
								<?php echo $mood_string;?>
			                </div>
					  	</div>
					</div>
					<?php
					}
					?>
                </div>
            </div>
		</div>
    </div>
</div>

<!-- Modal Form -->
<div id="form-primary" tabindex="-1" role="dialog" class="md-modal fade ">
    <iframe width="100%" height="100%" frameborder="0" scrolling="no" allowtransparency="true"></iframe>
</div>
<!-- Modal Form -->

<script type='text/javascript'>//<![CDATA[
$(window).load(function(){});//]]>
</script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/js/main.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.niftymodals/js/jquery.modalEffects.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#table1').DataTable( {
        "processing": true,
        "serverSide": false,
        "ajax": ""
    });

    $("#reload").click(function(){
        $("#table1").dataTable().fnDraw();
        });
} );
</script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dataTables();
    });
</script>

 <script type="text/javascript">
       function hideLoading() {
            document.getElementById('divLoading').style.display = "none";
            document.getElementById('divFrameHolder').style.display = "block";
        }
    </script>

<script>
$('a.external').on('click', function(e) {
        e.preventDefault();
        var src = $(this).attr('data-src');
        //var height = $(this).attr('data-height') || 300;
        //var width = $(this).attr('data-width') || 400;
        $(".md-modal iframe").attr({'src':src});
        //$(".md-modal iframe").attr({'src':src,'height': height,'width': width});
    });

$('#table1').on('click','a.external', function(e) {
        e.preventDefault();
        var src = $(this).attr('data-src');
        $(".md-modal iframe").attr({'src':src});
        $('#form-primary').addClass('md-show');
    });
</script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
  	//initialize the javascript
  	App.init();
  	App.uiNotifications();
  });
</script>
</body>
</html>
