<div class="am-content">
	<div class="page-head">
		<h2>Overview</h2>
	</div>
    <div class="main-content">
        <div class="row">
		</div>
    </div>
</div>

<!-- Modal Form -->
<div id="form-primary" tabindex="-1" role="dialog" class="md-modal fade ">
    <iframe width="100%" height="100%" frameborder="0" scrolling="no" allowtransparency="true"></iframe>
</div>
<!-- Modal Form -->

<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/js/main.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.niftymodals/js/jquery.modalEffects.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#table1').DataTable( {
        "processing": true,
        "serverSide": false,
        "ajax": ""
    });

    $("#reload").click(function(){
        $("#table1").dataTable().fnDraw();
        });
} );
</script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dataTables();
    });
</script>

 <script type="text/javascript">
       function hideLoading() {
            document.getElementById('divLoading').style.display = "none";
            document.getElementById('divFrameHolder').style.display = "block";
        }
    </script>

<script>
$('a.external').on('click', function(e) {
        e.preventDefault();
        var src = $(this).attr('data-src');
        //var height = $(this).attr('data-height') || 300;
        //var width = $(this).attr('data-width') || 400;
        $(".md-modal iframe").attr({'src':src});
        //$(".md-modal iframe").attr({'src':src,'height': height,'width': width});
    });

$('#table1').on('click','a.external', function(e) {
        e.preventDefault();
        var src = $(this).attr('data-src');
        $(".md-modal iframe").attr({'src':src});
        $('#form-primary').addClass('md-show');
    });
</script>
</body>
</html>
