<div class="am-content">
    <div class="page-head">
        <h2>User</h2>
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">User</li>
        </ol>
    </div>
    <div class="main-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="widget widget-fullwidth widget-small">
                    <div class="widget-head">
                    	<a class="md-trigger external" data-toggle="modal" data-modal="form-primary" data-src="<?php echo $GLOBALS["site_url"];?>/admin/usermodel/" data-target="#form-primary">
                    		<button data-modal="full-primary" class="btn btn-space btn-primary md-trigger">Add User</button>
                    	</a>
                        <a href="" id="reload" class="btn btn-space btn-primary">Reload</a>
                    </div>
                    <table id="table1" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr>
								<th>Name</th>
								<th>Email</th>
                                <th>Contact No.</th>
                                <th>Created at</th>
                                <th></th>
                            </tr>
                        </thead>
						<tbody>
                            <?php
                            foreach($users as $user)
                            {
                            ?>
                            <tr>
                                <td><?php echo $user['UserName'];?></td>
                                <td><?php echo $user['Email'];?></td>
                                <td><?php echo $user['Mobile'];?></td>
                                <td><?php echo $user['Created'];?></td>
                                <td>
                                    <a class="md-trigger external btn btn-default" data-toggle="modal" data-modal="form-primary" data-src="<?php echo $GLOBALS["site_url"];?>/admin/usermodel/edit/<?php echo $user['UserId'];?>/" data-target="#form-primary" title="Edit"><i class="icon s7-note2"></i></a>
                                    <a class="md-trigger external btn btn-default" data-toggle="modal" data-modal="form-primary" data-src="<?php echo $GLOBALS["site_url"];?>/admin/usermodel/reset/<?php echo $user['UserId'];?>/" data-target="#form-primary" title="Reset Password"><i class="icon s7-lock"></i></a>
                                    <a class="md-trigger external btn btn-default" data-toggle="modal" data-modal="form-primary" data-src="<?php echo $GLOBALS["site_url"];?>/admin/usermodel/delete/<?php echo $user['UserId'];?>/" data-target="#form-primary" title="Delete"><i class="icon s7-trash"></i></a>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Form -->
<div id="form-primary" tabindex="-1" role="dialog" class="md-modal fade ">
    <iframe width="100%" height="100%" frameborder="0" scrolling="no" allowtransparency="true"></iframe>
</div>
<!-- Modal Form -->

<script type='text/javascript'>//<![CDATA[
$(window).load(function(){});//]]>
</script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/js/main.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo $GLOBALS["site_url"]; ?>/assets/admin_assets/lib/jquery.niftymodals/js/jquery.modalEffects.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#table1').DataTable( {
        "processing": true,
        "serverSide": false,
        "ajax": ""
    });

    $("#reload").click(function(){
        $("#table1").dataTable().fnDraw();
        });
} );
</script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dataTables();
    });
</script>

 <script type="text/javascript">
       function hideLoading() {
            document.getElementById('divLoading').style.display = "none";
            document.getElementById('divFrameHolder').style.display = "block";
        }
    </script>

<script>
$('a.external').on('click', function(e) {
        e.preventDefault();
        var src = $(this).attr('data-src');
        //var height = $(this).attr('data-height') || 300;
        //var width = $(this).attr('data-width') || 400;
        $(".md-modal iframe").attr({'src':src});
        //$(".md-modal iframe").attr({'src':src,'height': height,'width': width});
    });

$('#table1').on('click','a.external', function(e) {
        e.preventDefault();
        var src = $(this).attr('data-src');
        $(".md-modal iframe").attr({'src':src});
        $('#form-primary').addClass('md-show');
    });
</script>
</body>
</html>
