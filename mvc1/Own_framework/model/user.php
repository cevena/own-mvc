<?php
class Userdata{
	var $obj_data;

    public function __construct()
    {
        $this->obj_data = new MySqlHelper();
    }

	// User activity at admin side
	public function getuser($id)
	{
		$users = $this->obj_data->GetRows("users","*","UserId='".$id."'");
		return $users;
	}

	// Add update delete phrases at admin side
	public function getphrases()
	{
		$phrase = $this->obj_data->GetRowsByQuery("select Phrase from phrases ORDER BY RAND() LIMIT 1");
		return $phrase;
	}

	public function addphrase($form_data)
	{
		$date=date('y-m-d');
		$form_data['Created'] = $date;

		$querydata['tablename'] = "phrases";
		$querydata['rowdata'] = $form_data;
		$querydata['identity'] = "PhraseId";

		return $this->obj_data->Insert($querydata);
	}

	public function editphrase($form_data)
	{
		$date=date('y-m-d');
		$form_data['Updated'] = '20'.$date;

		$querydata['tablename'] = "phrases";
		$querydata['rowdata'] = $form_data;
		$querydata['identity'] = "PhraseId";

		$this->obj_data->Update($querydata);
	}

	public function deletephrase($id)
	{
		$this->obj_data->Delete('phrases',"PhraseId=$id");
	}
}
