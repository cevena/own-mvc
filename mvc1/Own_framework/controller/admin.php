<?php
include_once("model/admin.php");

class Admin{
	public $model;

	public function __construct()
    {
		if(!isset($_SESSION['UserId']) OR $_SESSION['is_admin']=='0')
		{
		   header("location:".$GLOBALS["site_url"]);
		}
		$this->model = new Admindata();
    }

	public function defaultmethod()
	{
		$this->dashboard();
	}

	public function dashboard()
	{
		$title = 'Admin';
		include 'view/common/header.php';
		include 'view/admin/sub_header.php';
		include 'view/admin/sidebar.php';
		include 'view/admin/dashboard.php';
		include 'view/admin/footer.php';
	}

	public function users()
	{
		$title = 'Admin | Users';
		$users = $this->model->getuser();

		include 'view/common/header.php';
		include 'view/admin/sub_header.php';
		include 'view/admin/sidebar.php';
		include 'view/admin/users.php';
		include 'view/admin/footer.php';
	}

	public function usermodel($mode='',$id='')
	{
		if(!empty($mode) && !empty($id))
		{
			if($mode=='edit')
			{
				$edituser = $this->model->getuser($id);
				include 'view/admin/users_model.php';
			}
			if($mode=='reset')
			{
				include 'view/admin/reset_model.php';
			}
			if($mode=='delete')
			{
				include 'view/admin/users_model.php';
			}
		}
		else
		{
			include 'view/admin/users_model.php';
		}
	}

	public function adduser()
	{
		$form_data['UserName'] = $_POST['name'];
		$form_data['Email'] = $_POST['email'];
		$form_data['Mobile'] = $_POST['mobile'];
		$form_data['Password'] = md5($_POST['password']);

		$this->model->adduser($form_data);

		echo "<script> parent.$('#form-primary').modal('hide');
		parent.$('#table1').dataTable().fnDraw();
		</script>";
	}

	public function edituser($id)
	{
		$form_data['UserId'] = $id;
		$form_data['UserName'] = $_POST['name'];
		$form_data['Email'] = $_POST['email'];
		$form_data['Mobile'] = $_POST['mobile'];

		$this->model->edituser($form_data);
		echo "<script> parent.$('#form-primary').modal('hide');
		parent.$('#table1').dataTable().fnDraw();
		</script>";
	}

	public function resetpwd($id)
	{
		$form_data['UserId'] = $id;
		$form_data['Password'] = md5($_POST['password']);

		$this->model->resetpwd($form_data);

		echo "<script> parent.$('#form-primary').modal('hide');
		parent.$('#table1').dataTable().fnDraw();
		</script>";
	}

	public function deleteuser($id)
	{
		$this->model->deleteuser($id);
		echo "<script> parent.$('#form-primary').modal('hide');
		parent.$('#table1').dataTable().fnDraw();
		</script>";
	}

	// Add update detele phrases
	public function phrases()
	{
		$title = 'Admin | Phrases';
		$phrases = $this->model->getphrases();

		include 'view/common/header.php';
		include 'view/admin/sub_header.php';
		include 'view/admin/sidebar.php';
		include 'view/admin/phrases.php';
		include 'view/admin/footer.php';
	}

	public function phrasmodel($mode='',$id='')
	{
		if(!empty($mode) && !empty($id))
		{
			if($mode=='edit')
			{
				$editphrase = $this->model->getphrases($id);
				include 'view/admin/phrases_model.php';
			}
			if($mode=='delete')
			{
				include 'view/admin/phrases_model.php';
			}
		}
		else
		{
			include 'view/admin/phrases_model.php';
		}
	}

	public function addphrase()
	{
		$form_data['Phrase'] = $_POST['phrase'];
		$this->model->addphrase($form_data);

		echo "<script> parent.$('#form-primary').modal('hide');
		parent.$('#table1').dataTable().fnDraw();
		</script>";
	}

	public function editphrase($id)
	{
		$form_data['PhraseId'] = $id;
		$form_data['Phrase'] = $_POST['phrase'];

		$this->model->editphrase($form_data);
		echo "<script> parent.$('#form-primary').modal('hide');
		parent.$('#table1').dataTable().fnDraw();
		</script>";
	}

	public function deletephrase($id)
	{
		$this->model->deletephrase($id);
		echo "<script> parent.$('#form-primary').modal('hide');
		parent.$('#table1').dataTable().fnDraw();
		</script>";
	}

	public function logout()
	{
		unset($_SESSION['UserId']);
		unset($_SESSION['is_admin']);
		session_destroy();
		header("location:".$GLOBALS["site_url"]);
	}
}
