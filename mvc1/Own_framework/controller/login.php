<?php
include_once("model/login.php");

class Login {
	public $model;

	public function __construct()
    {

    }

	public function defaultmethod()
	{
		$this->login();
	}

    public function login()
	{
		if(isset($_SESSION['UserId']))
		{
		   if($_SESSION['is_admin']=='0'){
			   header("location:".$GLOBALS["site_url"]."/user/dashboard");
		   }
		   else{
			   header("location:".$GLOBALS["site_url"]."/admin/dashboard");
		   }
	   }
	   include 'view/index.php';
	}

    public function submit()
	{
        $email = $_POST['email'];
		$password = md5($_POST['password']);

        $this->model = new LoginData();
        $result = $this->model->submit($email, $password);

        if($result=="0"){
			header("location:".$GLOBALS["site_url"]."/user/dashboard");
        }
        elseif($result=="1"){
			header("location:".$GLOBALS["site_url"]."/admin/dashboard");
        }
		else{
			$_SESSION['msg'] = $result;
			header("location:".$GLOBALS["site_url"]);
		}
	}
}
