-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2016 at 06:43 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `framework`
--

-- --------------------------------------------------------

--
-- Table structure for table `phrases`
--

CREATE TABLE IF NOT EXISTS `phrases` (
  `PhraseId` int(11) NOT NULL,
  `Phrase` varchar(500) NOT NULL,
  `Created` date NOT NULL,
  `Updated` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phrases`
--

INSERT INTO `phrases` (`PhraseId`, `Phrase`, `Created`, `Updated`) VALUES
(3, 'Hello, what''s up %name% ?', '2016-07-18', '2016-07-18'),
(4, 'Hoping for a relaxing evening tonight, Hello %name%', '2016-07-18', '0000-00-00'),
(5, '%name% waiting to hear from you today...', '2016-07-18', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Mobile` varchar(12) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `is_admin` int(2) NOT NULL,
  `Created` date NOT NULL,
  `Updated` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserId`, `UserName`, `Email`, `Mobile`, `Password`, `is_admin`, `Created`, `Updated`) VALUES
(1, 'Abhishek jain', 'jateendk@gmail.com', '9669127964', 'e10adc3949ba59abbe56e057f20f883e', 1, '2016-07-19', '0000-00-00'),
(12, 'Prateek Yadav', 'prateek@gmail.com', '9562325625', '202cb962ac59075b964b07152d234b70', 0, '2016-07-18', '2016-07-18'),
(14, 'Rohit Kapoor', 'rohit@gmail.com', '9669127965', '81dc9bdb52d04dc20036dbd8313ed055', 0, '2016-07-18', '2016-07-18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phrases`
--
ALTER TABLE `phrases`
  ADD PRIMARY KEY (`PhraseId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phrases`
--
ALTER TABLE `phrases`
  MODIFY `PhraseId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
