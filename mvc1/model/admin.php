<?php
class Admindata{
	var $obj_data;

    public function __construct()
    {
        $this->obj_data = new MySqlHelper();
    }

	// Useractivity at admin side
	public function getuser($id='')
	{
		if(!empty($id))
		{
			$users = $this->obj_data->GetRows("users","*","UserId='".$id."'");
			return $users;
		}
		else
		{
			$users = $this->obj_data->GetRows("users","*","is_admin='0'");
			return $users;
		}
	}

	public function adduser($form_data)
	{
		$date=date('y-m-d');
		$form_data['Created'] = $date;

		$querydata['tablename'] = "users";
		$querydata['rowdata'] = $form_data;
		$querydata['identity'] = "UserId";

		return $this->obj_data->Insert($querydata);
	}

	public function edituser($form_data)
	{
		$date=date('y-m-d');
		$form_data['Updated'] = '20'.$date;

		$querydata['tablename'] = "users";
		$querydata['rowdata'] = $form_data;
		$querydata['identity'] = "UserId";

		$this->obj_data->Update($querydata);
	}

	public function resetpwd($form_data)
	{
		$date=date('y-m-d');
		$form_data['Updated'] = '20'.$date;

		$querydata['tablename'] = "users";
		$querydata['rowdata'] = $form_data;
		$querydata['identity'] = "UserId";

		$this->obj_data->Update($querydata);
	}

	public function deleteuser($id)
	{
		$this->obj_data->Delete('users',"UserId=$id");
	}

	// Add update delete phrases at admin side
	public function getphrases($id='')
	{
		if(!empty($id))
		{
			$phrases = $this->obj_data->GetRows("phrases","*","PhraseId='".$id."'");
			return $phrases;
		}
		else
		{
			$phrases = $this->obj_data->GetRowsByQuery("select * from phrases");
			return $phrases;
		}
	}
	//
	public function addphrase($form_data)
	{
		$date=date('y-m-d');
		$form_data['Created'] = $date;

		$querydata['tablename'] = "phrases";
		$querydata['rowdata'] = $form_data;
		$querydata['identity'] = "PhraseId";

		return $this->obj_data->Insert($querydata);
	}
	//
	public function editphrase($form_data)
	{
		$date=date('y-m-d');
		$form_data['Updated'] = '20'.$date;

		$querydata['tablename'] = "phrases";
		$querydata['rowdata'] = $form_data;
		$querydata['identity'] = "PhraseId";

		$this->obj_data->Update($querydata);
	}
	//
	public function deletephrase($id)
	{
		$this->obj_data->Delete('phrases',"PhraseId=$id");
	}
}
?>
