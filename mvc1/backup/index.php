<?php
	include_once("controller/home.php");
	include_once("config/MySqlHelper.php");
	include_once("route.php");

	$controller = new Controller();
	$controller->invoke();

	$route = new Route();

	$route->add('/');
	$route->add('/about');
	$route->add('/contact');

	echo "<pre>";
	print_r($route);

	$route->submit();

?>
