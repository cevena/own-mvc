<?php
Class MySqlHelper
{
	public $con;
	public $server="localhost";

	public $user="root";
	public $password="";
	public $db="mvc_trial";

	private $table_name ="";
	private $where_string ="";
	private $join_string ="";
	private $orderby_string ="";
	private $identity_field ="";

	function __construct()
	{
		$this->server="localhost";

		$this->user="root";
		$this->password="";
		$this->db="mvc_trial";
	}

	public function Insert($querydata)
	{
		$this->ResetVariables();
			//ResetVariables();
		//INSERT INTO tbl_demo (Demo_NAme) VALUES ('testing for 1'); SELECT LAST_INSERT_ID();
		$table_name =$querydata["tablename"];
		$row_data = array();
		$row_data =$querydata["rowdata"];
		//print_r($row_data);
		$identity_field =$querydata["identity"];
		$strsql ="";
		$strsql="INSERT INTO ".$table_name."(";
		$strfields = array();
		$strparams = array();
		$strvalues = array();
		  foreach ($row_data as $key => $value)
			{
 				$strfields[]=$key;
				$strparams[]=":$key";

			}
	       $strsql = $strsql.join(",", $strfields).")values(".join(",", $strparams).")";

		   try {
			     $dbh = new PDO('mysql:host='.$this->server.';dbname='.$this->db,$this->user, $this->password);
			} catch (PDOException $e) {
			    print "Error!: " . $e->getMessage() . "<br/>";
			    die();
			}

		   $stmt = $dbh->prepare($strsql);

			foreach ($row_data as $key => $value)
			{
				$stmt->bindValue(':'.$key ,$value, PDO::PARAM_STR);
			}
			$stmt->execute();
			$identity_value = $dbh->lastInsertId();
		    $dbh = null;
		return $identity_value;
	}

	public function Update($querydata)
	{
		$this->ResetVariables();
		//INSERT INTO tbl_demo (Demo_NAme) VALUES ('testing for 1'); SELECT LAST_INSERT_ID();
		$table_name =$querydata["tablename"];
		$row_data = array();
		$row_data =$querydata["rowdata"];
		$identity_field =$querydata["identity"];
		$strsql="UPDATE ".$table_name." SET ";
		$strfields = array();
		$strparams = array();
		$strvalues = array();
		  foreach ($row_data as $key => $value)
			{
				if($key !=$identity_field)
 				$strfields[]="$key=:$key";
			}
	       $strsql = $strsql.join(",", $strfields) ;
			$strsql = $strsql.' Where '.$identity_field.'=:'.$identity_field;

			$dbh = new PDO('mysql:host='.$this->server.';dbname='.$this->db,$this->user, $this->password);

			$stmt = $dbh->prepare($strsql);

			foreach ($row_data as $key => $value)
			{
				$stmt->bindValue(':'.$key ,$value, PDO::PARAM_STR);
			}
		$stmt->execute();

	    $dbh = null;
	}

	public function Delete($tablename,$where)
	{
		$this->ResetVariables();
		//INSERT INTO tbl_demo (Demo_NAme) VALUES ('testing for 1'); SELECT LAST_INSERT_ID();

		$strsql="DELETE FROM ".$tablename;
		if(!empty($where))
		{

			$strsql = $strsql.' Where '.$where;
		}
			$dbh = new PDO('mysql:host='.$this->server.';dbname='.$this->db,$this->user, $this->password);

			$stmt = $dbh->prepare($strsql);

			$stmt->execute();
		      $dbh = null;

	}

	public function GetRows($tablename,$columns,$where)
	{
		$dbh = new PDO('mysql:host='.$this->server.';dbname='.$this->db,$this->user, $this->password);
		$strsql = "Select ".$columns." From ".$tablename." Where ".$where;
		//echo $strsql;
		$rows = $dbh->query($strsql);
		$dbh = null;
		//print_r($rows);

		return $rows;
	}

	public function GetRow($tablename,$columns)
	{
		$dbh = new PDO('mysql:host='.$this->server.';dbname='.$this->db,$this->user, $this->password);
		$strsql = "Select ".$columns." From ".$tablename;
		//echo $strsql;
		$rows = $dbh->query($strsql);
		$dbh = null;
		return $rows;
	}

	public function GetSingle($tablename,$columns,$where)
	{
		try {

		$dbh = new PDO('mysql:host='.$this->server.';dbname='.$this->db,$this->user, $this->password);
		$strsql = "Select ".$columns." From ".$tablename." Where ".$where;
		//echo $strsql;
		$rows = $dbh->query($strsql);
		$f = $rows->fetch();
	    $result = $f[$columns];
		$dbh = null;
  		return $result;
		}
		catch(PDOException $e) {
	    	echo 'Connection failed: ' . $e->getMessage();
		    exit;
}
	}

	public function GetSingleByFormula($tablename,$formula,$columns,$where)
	{
		try {

		$dbh = new PDO('mysql:host='.$this->server.';dbname='.$this->db,$this->user, $this->password);
		$strsql = "Select ".$formula." From ".$tablename." Where ".$where;
		$rows = $dbh->query($strsql);
		$f = $rows->fetch();
	    $result = $f[$columns];


		$dbh = null;
  		return $result;
		}
		catch(PDOException $e) {
	    	echo 'Connection failed: ' . $e->getMessage();
		    exit;
}
	}

	public function GetRowsByQuery($query)
	{
		$dbh = new PDO('mysql:host='.$this->server.';dbname='.$this->db,$this->user, $this->password);
		//$strsql = "Select ".$columns." From ".$tablename." Where ".$where;
				//echo $strsql;
		$rows = $dbh->query($query);
		$dbh = null;
		return $rows;
	}
	/*public function GetRows($tablename,$columns,$join,$where)
	{

	}*/

	/*

	public function GetAll($querydata)
	{

	}
	public function GetSingleByQuery($querydata)
	{

	}
	public function GetSingleByQuery($querydata)
	{

	}
	*/

	private function ResetVariables()
	{
		$table_name ="";
		$where_string ="";
		$join_string ="";
		$orderby_string ="";
		$identity_field ="";
	}


}

?>
